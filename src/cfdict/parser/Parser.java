/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict.parser;

import cfdict.config.Config;
import cfdict.model.Sign;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.LinkedList;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class Parser {

    private Config config;
    private SAXParserFactory fabrique;
    private SAXParser parseur;

    public Parser(Config conf) {
        this.config = conf;
    }

    public LinkedList<Sign> process() throws ParserConfigurationException, SAXException, IOException {
        // création d'une fabrique de parseurs SAX
        fabrique = SAXParserFactory.newInstance();

        // création d'un parseur SAX
        parseur = fabrique.newSAXParser();

        // lecture d'un fichier XML avec un Handler approprié
        File fichier = new File("download/" + config.getFileName() + "/" + config.getFileUnzippedName());

        InputStream inputStream = new FileInputStream(fichier);
        Reader reader = new InputStreamReader(inputStream, "UTF-8");

        InputSource is = new InputSource(reader);
        is.setEncoding("UTF-8");

        SignHandler gestionnaire = new SignHandler();
        parseur.parse(is, gestionnaire);
        return gestionnaire.getResult();

    }
}
