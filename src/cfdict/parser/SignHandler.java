/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict.parser;

import cfdict.model.Sign;
import java.util.LinkedList;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SignHandler extends DefaultHandler {
    //résultats de notre parsing

    private LinkedList<Sign> dic;
    private LinkedList<String> translations;
    private Sign sign;
    //flags nous indiquant la position du parseur
    private boolean inDic, inWord, inId, inTrad, inSimple, inPinyin, inTimeUpd, inTransPart, inTrans;
    //buffer nous permettant de récupérer les données 
    private StringBuffer buffer;

    // simple constructeur
    public SignHandler() {
        super();
    }
    //détection d'ouverture de balise

    public void startElement(String uri, String localName,
            String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case "dic":
                dic = new LinkedList<Sign>();
                inDic = true;
                break;
            case "word":
                sign = new Sign();
                inWord = true;
                break;
            case "translations":
                translations = new LinkedList<String>();
                inTransPart = true;
                break;
            default:
                buffer = new StringBuffer();
                switch (qName) {
                    case "traditional":
                        inTrad = true;
                        break;
                    case "simplified":
                        inSimple = true;
                        break;
                    case "pinyin":
                        inPinyin = true;
                        break;
                    case "id":
                        inId = true;
                        break;
                    case "time_upd":
                        inTimeUpd = true;
                        break;
                    case "translation":
                        inTrans = true;
                        break;
                    default:
                        throw new SAXException("Balise " + qName + " inconnue.");
                }
                break;
        }
    }
    //détection fin de balise

    public void endElement(String uri, String localName, String qName)
            throws SAXException {
        switch (qName) {
            case "dic":
                inDic = false;
                break;
            case "word":
                dic.add(sign);
                sign = null;
                inWord = false;
                break;
            case "translations":
                sign.setTraductions(translations);
                inTransPart = false;
                break;
            case "translation":
                translations.add(buffer.toString());
                buffer = null;
                inTrans = false;
                break;
            case "traditional":
                sign.setTraditionnel(buffer.toString());
                buffer = null;
                inTrad = false;
                break;
            case "simplified":
                sign.setSimple(buffer.toString());
                buffer = null;
                inSimple = false;
                break;
            case "pinyin":
                sign.setPinyin(buffer.toString());
                buffer = null;
                inPinyin = false;
                break;
            case "id":
                sign.setId(Integer.parseInt(buffer.toString()));
                buffer = null;
                inId = false;
                break;
            case "time_upd":
                String value = buffer.toString();
                if (!value.equals("")) {
                    sign.setLastModif(Long.parseLong(value));
                } else {
                    sign.setLastModif(new Long(0));
                }
                buffer = null;
                inTimeUpd = false;
                break;
            default:
                //erreur, on peut lever une exception
                throw new SAXException("Balise " + qName + " inconnue.");
        }
    }
    //détection de caractères

    public void characters(char[] ch, int start, int length)
            throws SAXException {
        String lecture = new String(ch, start, length);
        if (buffer != null) {
            buffer.append(lecture);
        }
    }
    //début du parsing

    public void startDocument() throws SAXException {
        System.out.println("Debut du parsing");
    }
    //fin du parsing

    public void endDocument() throws SAXException {
        System.out.println("Fin du parsing");
    }

    public LinkedList<Sign> getResult() {
        return dic;
    }
}
