/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict.decompress;

import cfdict.config.Config;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class Decompress {

    private Config config;

    public Decompress(Config conf) {
        this.config = conf;
    }

    public void process() throws IOException {
        System.out.println("Debut de la decompression");

        String strZipFile = "download/" + config.getFileName() + ".zip";

        /*
         * STEP 1 : Create directory with the name of the zip file
         *
         * For e.g. if we are going to extract c:/demo.zip create c:/demo
         * directory where we can extract all the zip entries
         *
         */
        File fSourceZip = new File(strZipFile);
        String zipPath = strZipFile.substring(0, strZipFile.length() - 4);
        File temp = new File(zipPath);
        temp.mkdir();

        /*
         * STEP 2 : Extract entries while creating required
         * sub-directories
         *
         */
        ZipFile zipFile = new ZipFile(fSourceZip);
        Enumeration e = zipFile.entries();

        while (e.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) e.nextElement();
            File destinationFilePath = new File(zipPath, entry.getName());

            //create directories if required.
            destinationFilePath.getParentFile().mkdirs();

            //if the entry is directory, leave it. Otherwise extract it.
            if (entry.isDirectory()) {
                continue;
            } else {
                try (BufferedInputStream bis = new BufferedInputStream(zipFile
                        .getInputStream(entry))) {
                    int b;
                    byte buffer[] = new byte[1024];

                    /*
                     * read the current entry from the zip file, extract it
                     * and write the extracted file.
                     */
                    FileOutputStream fos = new FileOutputStream(destinationFilePath);
                    BufferedOutputStream bos = new BufferedOutputStream(fos,
                            1024);
                    while ((b = bis.read(buffer, 0, 1024)) != -1) {
                        bos.write(buffer, 0, b);
                    }

                    //flush the output stream and close it.
                    bos.flush();
                    bos.close();
                }
            }
        }
        System.out.println("Fin de la decompression");

    }
}
