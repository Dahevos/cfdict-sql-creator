/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict;

import cfdict.decompress.Decompress;
import cfdict.downloader.Download;
import cfdict.config.Config;
import cfdict.generator.Generator;
import cfdict.generator.InsertGenerator;
import cfdict.generator.UpdateGenerator;
import cfdict.parser.Parser;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class CFDICTSqlCreator {

    public static void main(String[] args) {

        try {
            Config config = new Config("config/config.properties");
            System.out.println("[MODE " + config.getMode() + "]");
            
            Download dl = new Download(config);
            dl.process();
            Decompress dp = new Decompress(config);
            dp.process();
            Parser p = new Parser(config);
            Generator g;
            if (config.getMode().equals("INSERT"))
                g = new InsertGenerator(config, p.process());
            else
                g = new UpdateGenerator(config, p.process());            
            g.process();
            
            
            

            System.out.println("Debut de la mise a jour du fichier de configuration");
            // set the timestamp for next time update process
            config.setTimestamp();
            // set the mode to update
            config.setModeUpdate();
            // save the modification
            config.updateConfigFile();
            System.out.println("Fin de la mise a jour du fichier de configuration");

            System.out.println("Execution terminee, vous pouvez fermer cette fenetre.");
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CFDICTSqlCreator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CFDICTSqlCreator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException | SAXException ex) {
            Logger.getLogger(CFDICTSqlCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
