/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cfdict.model;

import java.util.LinkedList;

/**
 *
 * @author dahev_000
 */
public class Sign {

    private int id;
    private String traditionnel;
    private String simple;
    private String pinyin;
    private Long lastModif;
    private LinkedList<String> traductions;

    public Sign() {
    }

    public Sign(int id, String traditionnel, String simple, String pinyin, Long lastModif, LinkedList<String> traductions) {
        this.id = id;
        this.traditionnel = traditionnel;
        this.simple = simple;
        this.pinyin = pinyin;
        this.lastModif = lastModif;
        this.traductions = traductions;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTraditionnel() {
        return traditionnel;
    }

    public void setTraditionnel(String traditionnel) {
        this.traditionnel = traditionnel;
    }

    public String getSimple() {
        return simple;
    }

    public void setSimple(String simple) {
        this.simple = simple;
    }

    public String getPinyin() {
        return pinyin;
    }

    public void setPinyin(String pinyin) {
        this.pinyin = pinyin;
    }

    public Long getLastModif() {
        return lastModif;
    }

    public void setLastModif(Long lastModif) {
        this.lastModif = lastModif;
    }

    public LinkedList<String> getTraductions() {
        return traductions;
    }

    public void setTraductions(LinkedList<String> traductions) {
        this.traductions = traductions;
    }
}
