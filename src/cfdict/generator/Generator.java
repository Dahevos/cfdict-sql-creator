/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ----------------------------------------------------------------------------*/
package cfdict.generator;

import cfdict.config.Config;
import cfdict.model.Sign;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public abstract class Generator {

    protected LinkedList<Sign> dic;
    protected Config conf;
    protected File file;
    protected FileWriter fw;
    protected BufferedWriter bw;
    protected int nbrFile;
    protected String content;

    public Generator(Config conf, LinkedList<Sign> dic) {
        this.conf = conf;
        this.dic = dic;
        this.nbrFile = 0;
        System.out.println("Debut de le generation");
    }

    protected void purgeOldFile() {
        File path = new File("result");
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                files[i].delete();
            }
        }
    }

    protected void close() throws IOException {
        bw.flush();
        System.out.println("Fin de le generation");

    }

    public void process() throws IOException {
        purgeOldFile();
    }
}
