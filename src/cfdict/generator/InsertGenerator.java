/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ----------------------------------------------------------------------------*/

package cfdict.generator;

import cfdict.config.Config;
import cfdict.model.Sign;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;


public class InsertGenerator extends Generator {

   
    public InsertGenerator(Config conf, LinkedList<Sign> dic) {
        super(conf, dic);
    }

   
    public void process() throws IOException {
        super.process();
        initSign();
        sign();
    }

    private void initSign() throws IOException {
        nbrFile++;
        file = new File("result/INSERT_" + nbrFile + ".sql");
        file.createNewFile();

        this.fw = new FileWriter(file.getAbsoluteFile());
        this.bw = new BufferedWriter(fw);

        if (this.nbrFile == 1) {
            content = "CREATE DATABASE IF NOT EXISTS " + conf.getDbName() + " character set utf8 COLLATE utf8_general_ci;\r\n";
            bw.write(content.toCharArray());
        }
        content = "USE " + conf.getDbName() + ";\r\n";
        bw.write(content.toCharArray());

        if (this.nbrFile == 1) {
            content = "CREATE TABLE " + conf.getSinoTable() + " ( "
                    + conf.getSinoTableIdName() + " INTEGER PRIMARY KEY,"
                    + conf.getSinoTableTraditionalName() + " VARCHAR(50),"
                    + conf.getSinoTableSimplifiedName() + " VARCHAR(50),"
                    + conf.getSinoTablePinyinName() + " VARCHAR(250) ); \r\n";
            bw.write(content.toCharArray());
        }

        content = "INSERT INTO " + conf.getSinoTable()
                + " ( "
                + conf.getSinoTableIdName() + ","
                + conf.getSinoTableTraditionalName() + ","
                + conf.getSinoTableSimplifiedName() + ","
                + conf.getSinoTablePinyinName()
                + " ) VALUES  ";
        bw.write(content.toCharArray());


    }

    private void sign() throws IOException {


        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {

            Sign sign = it.next();
            content = "('" + sign.getId() + "',"
                    + "'" + sign.getTraditionnel() + "', "
                    + "'" + sign.getSimple() + "', "
                    + "'" + sign.getPinyin() + "'"
                    + ")";

            bw.write(content.toCharArray());

            if (it.hasNext()) {
                if (file.length() > Integer.parseInt(conf.getMaxFileSize()) * 1048576) {
                    content = ";";
                    bw.write(content.toCharArray());
                    bw.flush();
                    initSign();
                } else {
                    content = ",";
                    bw.write(content.toCharArray());
                }
            }
        }
        content = ";\r\n";
        bw.write(content.toCharArray());
        bw.flush();

        initTrad(true);
        trad();


    }

    private void initTrad(boolean begining) throws IOException {
        if (!begining) {
            nbrFile++;
            file = new File("result/INSERT_" + nbrFile + ".sql");
            file.createNewFile();

            this.fw = new FileWriter(file.getAbsoluteFile());
            this.bw = new BufferedWriter(fw);

            content = "USE " + conf.getDbName() + ";\r\n";
            bw.write(content.toCharArray());
        } else {
            content = "CREATE TABLE " + conf.getTradTable() + " ( "
                    + conf.getTradTableIdName() + " INTEGER PRIMARY KEY AUTO_INCREMENT,"
                    + conf.getTradTableIdSignName() + " INTEGER REFERENCES " + conf.getSinoTable() + "(id),"
                    + conf.getTradTableContentName() + " VARCHAR(250)); \r\n"
                    + "CREATE INDEX idSignIndex ON " + conf.getTradTable() + " (" + conf.getTradTableIdSignName() + ");\r\n";

            bw.write(content.toCharArray());
        }

        content = "INSERT INTO " + conf.getTradTable()
                + " ( "
                + conf.getTradTableIdSignName() + ", "
                + conf.getTradTableContentName()
                + " ) VALUES ";
        bw.write(content.toCharArray());


    }

    private void trad() throws IOException {
        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {
            Sign sign = it.next();

            for (Iterator<String> it2 = sign.getTraductions().iterator(); it2.hasNext();) {
                String trad = it2.next();

                content = "('" + sign.getId() + "',"
                        + "'" + trad.replace("'", "\\'") + "'"
                        + ")";

                if (it2.hasNext()) {
                    content += ",";
                }
                bw.write(content.toCharArray());
            }


            if (it.hasNext()) {
                if (file.length() > Integer.parseInt(conf.getMaxFileSize()) * 1048576) {
                    content = ";";
                    bw.write(content.toCharArray());
                    bw.flush();
                    initTrad(false);
                } else {
                    content = ",";
                    bw.write(content.toCharArray());
                }
            }
        }
        content = ";\r\n";
        bw.write(content.toCharArray());
        close();
    }

}
