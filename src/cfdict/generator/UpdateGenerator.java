/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 ----------------------------------------------------------------------------*/
package cfdict.generator;

import cfdict.config.Config;
import cfdict.model.Sign;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;

public class UpdateGenerator extends Generator {

    public UpdateGenerator(Config conf, LinkedList<Sign> dic) {
        super(conf, dic);
    }

    public void process() throws IOException {
        super.process();
        purgeUnmodifiedSign();
        if (dic.size() > 0) {
            initSign();
            sign();
        }
    }

    private void purgeUnmodifiedSign() {
        
        LinkedList<Sign> newDic = new LinkedList<Sign>();
        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {
            Sign sign = it.next();
            if (sign.getLastModif() > Long.parseLong(conf.getTimestamp())) {
                newDic.add(sign);
            }
        }
        
        this.dic = newDic;
    }

    private void initSign() throws IOException {
        nbrFile++;
        file = new File("result/UPDATE_" + nbrFile + ".sql");
        file.createNewFile();

        this.fw = new FileWriter(file.getAbsoluteFile());
        this.bw = new BufferedWriter(fw);


        content = "USE " + conf.getDbName() + ";\r\n";
        bw.write(content.toCharArray());

        bw.flush();


    }

    private void sign() throws IOException {


        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {

            Sign sign = it.next();

            content = "UPDATE " + conf.getSinoTable()
                    + " SET " + conf.getSinoTableTraditionalName() + " = '" + sign.getTraditionnel() + "', "
                    + conf.getSinoTableSimplifiedName() + " = '" + sign.getSimple() + "', "
                    + conf.getSinoTablePinyinName() + " = '" + sign.getPinyin() + "'"
                    + " WHERE " + conf.getSinoTableIdName() + " = '" + sign.getId() + "'"
                    + "; \r\n";
            bw.write(content.toCharArray());


            if (file.length() > Integer.parseInt(conf.getMaxFileSize()) * 1048576) {
                bw.flush();
                initSign();
            }


        }

        bw.flush();
        initDeleteTrad(true); 
        deleteTrad();

    }

    private void initDeleteTrad(boolean begining) throws IOException {
        if (!begining) {
            nbrFile++;
            file = new File("result/UPDATE_" + nbrFile + ".sql");
            file.createNewFile();

            this.fw = new FileWriter(file.getAbsoluteFile());
            this.bw = new BufferedWriter(fw);

            content = "USE " + conf.getDbName() + ";\r\n";
            bw.write(content.toCharArray());
        }

        content = "DELETE FROM " + conf.getTradTable()
                + " WHERE " + conf.getTradTableIdName() + " IN (";
        bw.write(content.toCharArray());


    }

    private void deleteTrad() throws IOException {
        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {
            Sign sign = it.next();



            content = "'" + sign.getId() + "'";
            bw.write(content.toCharArray());





            if (it.hasNext()) {
                if (file.length() > Integer.parseInt(conf.getMaxFileSize()) * 1048576) {
                    content = ");";
                    bw.write(content.toCharArray());
                    bw.flush();
                    initDeleteTrad(false);
                } else {
                    content = ",";
                    bw.write(content.toCharArray());
                }
            }
        }
        content = ");\r\n";
        bw.write(content.toCharArray());
        bw.flush();
        initTrad(true);
        trad();
    }

    private void initTrad(boolean begining) throws IOException {
        if (!begining) {
            nbrFile++;
            file = new File("result/UPDATE_" + nbrFile + ".sql");
            file.createNewFile();

            this.fw = new FileWriter(file.getAbsoluteFile());
            this.bw = new BufferedWriter(fw);

            content = "USE " + conf.getDbName() + ";\r\n";
            bw.write(content.toCharArray());
        }

        content = "INSERT INTO " + conf.getTradTable()
                + " ( "
                + conf.getTradTableIdSignName() + ", "
                + conf.getTradTableContentName()
                + " ) VALUES ";
        bw.write(content.toCharArray());


    }

    private void trad() throws IOException {
        for (Iterator<Sign> it = dic.iterator(); it.hasNext();) {
            Sign sign = it.next();

            for (Iterator<String> it2 = sign.getTraductions().iterator(); it2.hasNext();) {
                String trad = it2.next();

                content = "('" + sign.getId() + "',"
                        + "'" + trad.replace("'", "\\'") + "'"
                        + ")";

                if (it2.hasNext()) {
                    content += ",";
                }
                bw.write(content.toCharArray());
            }


            if (it.hasNext()) {
                if (file.length() > Integer.parseInt(conf.getMaxFileSize()) * 1048576) {
                    content = ";";
                    bw.write(content.toCharArray());
                    bw.flush();
                    initTrad(false);
                } else {
                    content = ",";
                    bw.write(content.toCharArray());
                }
            }
        }
        content = ";\r\n";
        bw.write(content.toCharArray());
        close();
    }
}
