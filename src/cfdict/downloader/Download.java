/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict.downloader;

import cfdict.config.Config;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;


public class Download {

    private Config config;

    public Download(Config conf) {
        this.config = conf;
    }

    public void process() throws IOException {
        System.out.println("Debut du telechargement");
        InputStream input = null;
        FileOutputStream writeFile = null;

        try {
            URL url = new URL(config.getAdrFile());
            URLConnection connection = url.openConnection();
            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                throw new IOException("Invalide URL or file.");
            }

            input = connection.getInputStream();
            String fileName = "download/" + config.getFileName() + ".zip";
            writeFile = new FileOutputStream(fileName);
            byte[] buffer = new byte[1024];
            int read;

            while ((read = input.read(buffer)) > 0) {
                writeFile.write(buffer, 0, read);
            }
            writeFile.flush();
                        
        } finally {
            if (writeFile != null) {
                writeFile.close();
            }
            if (input != null) {
                input.close();
            }
        }
        System.out.println("Fin du telechargement");
    }

    
}

