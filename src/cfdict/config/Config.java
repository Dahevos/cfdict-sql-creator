/* ----------------------------------------------------------------------------

 CFDICT Parser
 Copyright (C) 2013  LENTINI Sébastien

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.


 ---------------------------------------------------------------------------- */
package cfdict.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class Config {

    private LinkedProperties properties;

    public Config(String path) throws FileNotFoundException, IOException {

        this.properties = new LinkedProperties();

        try (FileInputStream input = new FileInputStream(path)) {
            this.properties.load(input);
        }

    }

    public String getDbName() {
        return properties.getProperty("DB_NAME", "DB_DICTIONNAIRE");
    }

    public String getSinoTable() {
        return properties.getProperty("SINO_TABLE", "Sinogramme");
    }

    public String getSinoTableIdName() {
        return properties.getProperty("SINO_TABLE_ID_NAME", "id");
    }

    public String getSinoTablePinyinName() {
        return properties.getProperty("SINO_TABLE_PINYIN_NAME", "pinyin");
    }

    public String getSinoTableSimplifiedName() {
        return properties.getProperty("SINO_TABLE_SIMPLIFIED_NAME", "simplified");
    }

    public String getSinoTableTraditionalName() {
        return properties.getProperty("SINO_TABLE_TRADITIONAL_NAME", "traditional");
    }

    public String getTradTable() {
        return properties.getProperty("TRAD_TABLE", "Traduction");
    }

    public String getTradTableIdName() {
        return properties.getProperty("TRAD_TABLE_ID_NAME", "id");
    }

    public String getTradTableIdSignName() {
        return properties.getProperty("TRAD_TABLE_IDSIGN_NAME", "idSign");
    }

    public String getTradTableContentName() {
        return properties.getProperty("TRAD_TABLE_CONTENT_NAME", "content");
    }

    public String getAdrFile() {
        return properties.getProperty("ADR_FILE", "http://www.chine-informations.com/chinois/open/CFDICT/download.php?file=xml");
    }

    public String getFileName() {
        return properties.getProperty("FILE_NAME", "cfdict_xml");
    }

    public String getFileUnzippedName() {
        return properties.getProperty("FILE_UNZIPPED_NAME", "cfdict.xml");
    }

    public String getMaxFileSize() {
        return properties.getProperty("MAX_FILE_SIZE", "5");
    }

    public String getMode() {
        return properties.getProperty("MODE", "INSERT");
    }

    public void setModeUpdate() {
        properties.setProperty("MODE", "UPDATE");
    }

    public String getTimestamp() {
        return properties.getProperty("TIMESTAMP", "0");
    }

    public void setTimestamp() {
        properties.setProperty("TIMESTAMP", new Long(System.currentTimeMillis()/1000L).toString());
    }

    public void updateConfigFile() throws FileNotFoundException, IOException  {
        File f = new File("config/config.properties");
        OutputStream out = new FileOutputStream(f);
        properties.store(out, "# ----------------------------------------------------------------------------\n" +
"\n" +
"#    CFDICT Parser\n" +
"#    Copyright (C) 2013  LENTINI Sébastien\n" +
"\n" +
"#    This program is free software: you can redistribute it and/or modify\n" +
"#    it under the terms of the GNU General Public License as published by\n" +
"#    the Free Software Foundation, either version 3 of the License, or\n" +
"#    (at your option) any later version.\n" +
"\n" +
"#    This program is distributed in the hope that it will be useful,\n" +
"#    but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
"#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n" +
"#    GNU General Public License for more details.\n" +
"\n" +
"#    You should have received a copy of the GNU General Public License\n" +
"#    along with this program.  If not, see <http://www.gnu.org/licenses/>.\n" +
"\n" +
"\n" +
"# ----------------------------------------------------------------------------\n" +
"\n" +
"# Properties file\n" +
"# Ces informations restent confidentielles et ne sont pas transmises sur internet.\n" +
"# Elles permettent uniquement de générer le script sql.\n" +
"\n" +
"# ADR_FILE est l'adresse de téléchargement du dictionnaire CFDICT au format xml\n" +
"# FILE_NAME est le nom du fichier qui va être téléchargé sur votre disque dur\n" +
"# FILE_UNZIPPED_NAME est le nom final du fichier unzipper.\n" +
"# MAX_FILE_SIZE est la taille approximativement maximale d'une partie du fichier final en Mo. \n" +
"# Très utile pour splitter le fichier d'insertion ou d'update en plusieurs morceaux\n" +
"\n" +
"# MODE est à spécifier entre INSERT et UPDATE. Update est utilisé pour mettre à jour \n" +
"# une base existante créé via ce script (ou compatible avec...)\n" +
"# TIMESTAMP est la valeur du timestamp de la dernière exécution du script (permet de faciliter le mode UPDATE).\n" +
"# ne pas modifier cette valeur sauf si vous savez ce que vous faites !!!!!\n" +
"\n" +
"# DB_NAME est le nom de la base de données\n" +
"\n" +
"# SINO_TABLE est le nom de la table qui contiendra tout les sinogrammes.\n" +
"# SINO_TABLE_ID_NAME est le nom de la colonne pour l'id des signes\n" +
"# SINO_TABLE_TRADITIONAL_NAME est le nom de la colonne pour les signes du chinois traditionel\n" +
"# SINO_TABLE_SIMPLIFIED_NAME est le nom de la colonne pour les signes du chinois simplifié\n" +
"# SINO_TABLE_PINYIN_NAME est le nom de la colonne pour le pinyin des signes chinois\n" +
"\n" +
"# TRAD_TABLE est le nom de la table qui contiendra toutes les traductions\n" +
"# TRAD_TABLE_ID_NAME est le nom de la colonne pour l'id des traductions \n" +
"# TRAD_TABLE_IDSIGN_NAME est le nom de la colonne pour l'id du sign de la traduction \n" +
"# TRAD_TABLE_CONTENT_NAME est le nom de la colonne pour la traduction du signe chinois\n" +
"\n" +
"\n" +
"");
    }
}
