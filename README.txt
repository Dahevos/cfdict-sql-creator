CFDICT SQL CREATOR
Par LENTINI S�bastien
cfdict.sql.creator@gmail.com

Ce programme est sous la license Apache V2.0 et permet de transformer le dictionnaire 
CFDICT (http://www.chine-informations.com/chinois/open/CFDICT/) au format SQL. 
Aucune garantie de resultat n'est donnee.

Le but de ce projet est de :
- telecharger automatiquement la derniere version du dictionnaire CFDICT
- creer un fichier d'insertion de script SQL pour une nouvelle base
- creer un fichier de mise a jour de script SQL pour une base existante 
- pouvoir personnaliser un certain nombre de choses dans cette generation
- D�couper chaque fichier en taille maximale permettant de faciliter l'insertion 
et la mise � jour de vos bases de donn�es.


Pour savoir comment utiliser ce programme, merci de vous reporter au Wiki : 
https://bitbucket.org/Dahevos/cfdict-sql-creator/wiki/Home

Pour tout report de bugs/suggestions, m'envoyer un e-mail a cfdict.sql.creator@gmail.com